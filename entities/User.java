package entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="USERS")
public class User implements Serializable
{
	public enum AccountType 
	{
		Professor,
		Dean
	}
	@Id
	@TableGenerator(name = "UserGenerator")
	@GeneratedValue(generator = "UserGenerator")
	@Column(name="USER_ID")
	private int id;
	private Teacher teacher;
    public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	private String username;
    private String password;
    private AccountType type;


	public int getId() {
		return id;
	}
    
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
    
    public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}
}
