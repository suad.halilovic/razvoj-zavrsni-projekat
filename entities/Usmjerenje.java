package entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name="SUBJECTS")
public class Usmjerenje implements Serializable
{

	@Id
	@TableGenerator(name = "UsmjerenjeGenerator")
	@GeneratedValue(generator = "UsmjerenjeGenerator")
	@Column(name="USMJERENJE_ID")
	private int id;
    private String name;
    private Collection<Subject> subjects;


}
