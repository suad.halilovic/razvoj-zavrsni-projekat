package view.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Vector;
import java.time.chrono.*;

import javafx.collections.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import api.Api;
import entities.*;

public class AlternativeStudentSceneController extends ShowConfirmDialogWrapper{
	public static AlternativeStudentSceneController instance = null;
	public static void reinitFilters() {
		instance.initFilters();
	}
	@FXML
	public Button btnLogout;
	@FXML
	public Button btnReset;
	@FXML
	public DatePicker datePicker;
	@FXML
	public GridPane raspored;

	@FXML
	public Label lblDatePon;
	@FXML
	public Label lblDateUto;
	@FXML
	public Label lblDateSri;
	@FXML
	public Label lblDateCet;
	@FXML
	public Label lblDatePet;
	@FXML
	public Label lblDateSub;

	@FXML
	public ChoiceBox<Building>  filterBuilding;
	@FXML
	public ChoiceBox<Classroom> filterClassroom;
	@FXML
	public ChoiceBox<Teacher> 	filterProfessor;
	@FXML
	public ChoiceBox<String>	filterSemester;
	@FXML
	public ChoiceBox<String>	filterYear;
	@FXML
	public ChoiceBox<Subject> 	filterSubject;
	@FXML
	public ChoiceBox<String> 	filterGroup;
	
	// Helpers
	public GridPane previousPane = null;
	public Vector<Vector<HBox[]>> dani;
	
	public HBox[] hboxes;

	@FXML
	public HBox hboxPon;
	@FXML
	public HBox hboxUto;
	@FXML
	public HBox hboxSri;
	@FXML
	public HBox hboxCet;
	@FXML
	public HBox hboxPet;
	@FXML
	public HBox hboxSub;

	@FXML
	public ScrollPane scrollPanePon;
	@FXML
	public ScrollPane scrollPaneUto;
	@FXML
	public ScrollPane scrollPaneSri;
	@FXML
	public ScrollPane scrollPaneCet;
	@FXML
	public ScrollPane scrollPanePet;
	@FXML
	public ScrollPane scrollPaneSub;

	public void addNewGrid(Vector<HBox[]> vek, HBox box) {
		try {		
			GridPane grid =  FXMLLoader.load(getClass().getResource("../Grid.fxml"));
			HBox[] cells = new HBox[12];
			for(int i=0; i<12; i++) {
				HBox hbox = new HBox();
				grid.add(hbox, 0, i);
				cells[i] = hbox;
			}
			vek.add(cells);
			box.getChildren().add(grid);
		}catch(Throwable e) 
		{
			e.printStackTrace();
		}
		
	}
	@FXML
	public void initialize() {
		initStudentController();
	}
	public void initStudentController() 
	{
		if(instance == null)
			instance = this;

		// init dates
		LocalDate time = LocalDate.of(2019,  10,  10);
		datePicker.setChronology(Chronology.ofLocale(Locale.GERMANY));
		datePicker.setValue(time);
		setDates(time);
		
		// init all data in filters
		initFilters();
		
		initHBoxes();
		Collection<Cas> casovi = Api.getCasovi();
		renderCasovi(casovi);
	}
	
	@FXML
	private void handleButtonLogout(ActionEvent event) {
	    try {
		    Stage appStage=(Stage)btnLogout.getScene().getWindow();
	        Parent root=FXMLLoader.load(getClass().getResource("../LoginScene.fxml"));
	        Scene scene=new Scene(root);
	        scene.getStylesheets().add("view/stylesheet.css");
	        appStage.setScene(scene);
	        appStage.show();
	    }catch(IOException e) {
	    	e.printStackTrace();
	    }
	}
	
	
	@FXML
	private void handleDatePicker(ActionEvent event) {
		setDates(datePicker.getValue());
	}

	@FXML
	public void handleFilterStudent(ActionEvent event) 
	{
		try{
			ChoiceBox<Object> source = (ChoiceBox<Object>)event.getSource();
			
			if(source.getId().equals("filterBuilding")) 
			{
				 Classroom c = filterClassroom.getValue();
				 
				 if(filterBuilding.getValue() != null) {
					 setFilterClassroomList(FXCollections.observableArrayList(Api.getClassroomsOfBuilding(filterBuilding.getValue())));
					 // make sure the already selected classroom doesn't get reset 
					 if(c != null && filterBuilding.getValue().getId() == c.getBuilding().getId())
					 {
						 filterClassroom.setValue(c);
					 }
				 }
				 else {
					 setFilterClassroomList(FXCollections.observableArrayList(Api.getClassrooms()));
					 if(c != null)
					 {
						 filterClassroom.setValue(c);
					 }
				 }

			}
			else if(source.getId().equals("filterClassroom")) 
			{
				System.out.println(filterClassroom.getValue());
			}
			else if(source.getId().equals("filterProfessor")) 
			{
				System.out.println(filterProfessor.getValue());
			}
			else if(source.getId().equals("filterSemester")) 
			{
				System.out.println(filterSemester.getValue());
			}
			else if(source.getId().equals("filterSubject")) 
			{
				System.out.println(filterSubject.getValue());
			}
			else if(source.getId().equals("filterGroup")) 
			{
				System.out.println(filterGroup.getValue());
			}
		}catch(Throwable e) {}
		Collection<Cas> casovi = Api.getCasovi();

		if(filterBuilding.getValue() != null) {
			casovi = Api.filterCasByBuilding(casovi, filterBuilding.getValue());
		}
		
		if(filterClassroom.getValue() != null) {
			casovi = Api.filterCasByClassroom(casovi, filterClassroom.getValue());
		}

		if(filterProfessor.getValue() != null) {
			casovi = Api.filterCasByProfessor(casovi, filterProfessor.getValue());
		}

		if(filterSubject.getValue() != null) {
			casovi = Api.filterCasBySubject(casovi, filterSubject.getValue());
		}

		if(filterSemester.getValue() != null) {
			casovi = Api.filterCasBySemester(casovi, filterSemester.getValue());
		}
		if(filterYear.getValue() != null) {
			casovi = Api.filterCasByYear(casovi, filterYear.getValue());
		}

		if(filterGroup.getValue() != null) {
			casovi = Api.filterCasByGroup(casovi, stringToGroup(filterGroup.getValue()));
		}

		renderCasovi(casovi);
	}

	public Group.GroupType stringToGroup(String str){
		if(str.equals("Predavanje"))
			return Group.GroupType.PRED;
		else if(str.equals("AV"))
			return Group.GroupType.AV;
		else 
			return Group.GroupType.LV;
	}
	private void initHBoxes() {

		hboxes = new HBox[6];
		hboxes[0] = hboxPon;
		hboxes[1] = hboxUto;
		hboxes[2] = hboxSri;
		hboxes[3] = hboxCet;
		hboxes[4] = hboxPet;
		hboxes[5] = hboxSub;
		
		dani = new Vector<Vector<HBox[]>>(6);

		for(int i = 0; i < 6; i++) {
			dani.add(new Vector<HBox[]>());
		}

		addNewGrid(dani.elementAt(0), hboxPon);
		addNewGrid(dani.elementAt(1), hboxUto);
		addNewGrid(dani.elementAt(2), hboxSri);
		addNewGrid(dani.elementAt(3), hboxCet);
		addNewGrid(dani.elementAt(4), hboxPet);
		addNewGrid(dani.elementAt(5), hboxSub);
	}

	public void clearHBoxes() {
		for(Vector<HBox[]> vekDan : dani) {
			for(HBox[] cells : vekDan) {
				for(int j = 0; j < 12; j++) {
					cells[j].getChildren().clear();
					cells[j].getStyleClass().clear();
				}
			}
		}
		for(HBox box : hboxes)
			box.getChildren().clear();
		initHBoxes();
	}
	public int dayToInt(Cas.Day day) {
		switch(day) {
		case PON:
			return 0;
		case UTO:
			return 1;
		case SRI:
			return 2;
		case CET:
			return 3;
		case PET:
			return 4;
		case SUB:
			return 5;
		default:
			return -1;
		}
	}

	public Boolean canFit(Cas cas, HBox[] cells) {
		for(int i = 0; i < cas.getDuration(); i++) {
			if(cells[cas.getStartHour() + i - 8].getChildren().size() == 1)
				return false;
		}
		return true;
	}
	
	public void renderCasovi(Collection<Cas> casovi) {
		// first clear old values
		clearHBoxes();

		// render new ones
		for(Cas cas : casovi)
		{
			int dayAsNum = dayToInt(cas.getDay());
			Vector<HBox[]> cellsOfDay = dani.elementAt(dayAsNum);
			HBox hboxOfDay = hboxes[dayAsNum];

			
			HBox[] cells = null;
			for(HBox[] c : cellsOfDay) {
				if(canFit(cas, c)) {
					cells = c;
					break;
				}
			}
			if(cells == null) {
				addNewGrid(cellsOfDay, hboxOfDay);
				cells = cellsOfDay.lastElement();
			}

			VBox vbox = new VBox();


			vbox.getStyleClass().add("top");
			switch(cas.getGroup().getType()) {
			case PRED:
				vbox.getStyleClass().add("pred");
				break;
			case AV:
				vbox.getStyleClass().add("av");
				break;
			case LV:
				vbox.getStyleClass().add("lv");
				break;
			}

			Label lbl = new Label();
			lbl.setText(cas.getGroup().getSubject().getName());
			lbl.setAlignment(Pos.TOP_CENTER);
			lbl.setWrapText(true);
			lbl.setFont(new Font(11));
			lbl.setPrefWidth(10000);
			Label lbl2 = new Label();
			lbl2.setText(cas.getClassroom().getName());
			lbl2.setFont(new Font(11));
			lbl2.setAlignment(Pos.TOP_CENTER);
			lbl2.setPrefWidth(10000);
			lbl2.setWrapText(true);


			for(int i = 1; i < cas.getDuration(); i++) {
				VBox box = new VBox();
				Label label = new Label("");
				label.setPrefWidth(1000);
				label.setPrefHeight(1000);
				box.getChildren().add(label);
				if(i != cas.getDuration() - 1) {
					try {
						box.getStyleClass().add("mid");
					}catch(Throwable e) {e.printStackTrace();}
					
				}else {
					try {
						box.getStyleClass().add("bot");
					}catch(Throwable e) {e.printStackTrace();}
				}
				switch(cas.getGroup().getType()) {
				case PRED:
					box.getStyleClass().add("pred");
					break;
				case AV:
					box.getStyleClass().add("av");
					break;
				case LV:
					box.getStyleClass().add("lv");
					break;
				}
				cells[cas.getStartHour() - 8 + i].getChildren().add(box);
			}

			vbox.getChildren().add(lbl);
			vbox.getChildren().add(lbl2);
			cells[cas.getStartHour() - 8].getChildren().addAll(vbox);
		}
		
		disableScrollbars();
	}
	public void disableScrollbars() {
		// disable scrollbar if only one row
		for(int i=0; i< dani.size(); i++) {
			ScrollPane pane = getPaneFromIndex(i);
			if(dani.elementAt(i).size() > 1) {
				pane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
				pane.setHmin(0);
				pane.setHmax(1);
				pane.setHvalue(0);
				
			}else {
				pane.setHbarPolicy(ScrollBarPolicy.NEVER);
				pane.setHmin(0);
				pane.setHmax(0);
				pane.setHvalue(0);
			}
		}
		
	}
	private ScrollPane getPaneFromIndex(int i) {
		switch(i) {
		case 0:
			return scrollPanePon;
		case 1:
			return scrollPaneUto;
		case 2:
			return scrollPaneSri;
		case 3:
			return scrollPaneCet;
		case 4:
			return scrollPanePet;
			default:
			return scrollPaneSub;
		}
	}
	private void initFilters() {
		Collection<Building> buildings = Api.getBuildings();
		setFilterBuildingsList(FXCollections.observableArrayList(buildings));
		
		Collection<Classroom> classrooms = Api.getClassrooms();
		setFilterClassroomList(FXCollections.observableArrayList(classrooms));
		
		Collection<Teacher> teachers = Api.getTeachers();
		setFilterProfessorsList(FXCollections.observableArrayList(teachers));
		
		Collection<Subject> subjects = Api.getSubjects();
		setFilterSubjectsList(FXCollections.observableArrayList(subjects));
		
		setFilterGroupList();
		setFilterSemesterList();
		setFilterYearList();
	}
	
	private void setFilterBuildingsList(ObservableList<Building> list)
	{
		filterBuilding.setItems(list);
		filterBuilding.getItems().add(0, null);
	}
	
	private void setFilterClassroomList(ObservableList<Classroom> list)
	{
		filterClassroom.setItems(list);
		filterClassroom.getItems().add(0, null);
	}

	
	private void setFilterProfessorsList(ObservableList<Teacher> list)
	{
		filterProfessor.setItems(list);
		filterProfessor.getItems().add(0, null);
	}
	
	private void setFilterSubjectsList(ObservableList<Subject> list)
	{
		filterSubject.setItems(list);
		filterSubject.getItems().add(0, null);
	}

	private void setFilterGroupList()
	{
		ObservableList<String> list = FXCollections.observableArrayList();
		list.add("Predavanje");
		list.add("AV");
		list.add("LV");
		filterGroup.setItems(list);
		filterGroup.getItems().add(0, null);
	}
	private void setFilterYearList()
	{
		ObservableList<String> list = FXCollections.observableArrayList();
		list.add("I");
		list.add("II");
		list.add("III");
		list.add("IV");
		filterYear.setItems(list);
		filterYear.getItems().add(0, null);
	}
	private void setFilterSemesterList()
	{
		ObservableList<String> list = FXCollections.observableArrayList();
		list.add("I");
		list.add("II");
		list.add("III");
		list.add("IV");
		list.add("V");
		list.add("VI");
		list.add("VII");
		list.add("VIII");
		filterSemester.setItems(list);
		filterSemester.getItems().add(0, null);
	}
	
	// sets all the dates up (PON, UTO...) with correct dates and colors
	// for any given date
	public void setDates(LocalDate time) 
	{
		// reset previously colored pane to default
		if(previousPane != null) 
		{
			previousPane.setStyle("-fx-background-color:#EFEFEF");
			
			for(Node lbl : previousPane.getChildren()) 
			{
				((Label)lbl).setTextFill(Color.web("000000"));
			}
		}
		
		GridPane parent = null;
		int dayOfWeek = time.getDayOfWeek().getValue() - 1;
		
		if(dayOfWeek == 0)
			parent = ((GridPane)lblDatePon.getParent());
		else if(dayOfWeek == 1)
			parent = ((GridPane)lblDateUto.getParent());
		else if(dayOfWeek == 2)
			parent = ((GridPane)lblDateSri.getParent());
		else if(dayOfWeek == 3)
			parent = ((GridPane)lblDateCet.getParent());
		else if(dayOfWeek == 4)
			parent = ((GridPane)lblDatePet.getParent());
		else if(dayOfWeek == 5)
			parent = ((GridPane)lblDateSub.getParent());
		
		if(parent != null) 
		{
			parent.setStyle("-fx-background-color:#666666");
			
			for(Node lbl : parent.getChildren()) 
			{
				((Label)lbl).setTextFill(Color.web("ffffff"));
			}
		}
		
		// reset date to beginning of week (monday)
		time = time.minusDays(dayOfWeek);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
		
		// write all the dates
		lblDatePon.setText(time.format(format).toString());
		lblDateUto.setText(time.plusDays(1).format(format).toString());
		lblDateSri.setText(time.plusDays(2).format(format).toString());
		lblDateCet.setText(time.plusDays(3).format(format).toString());
		lblDatePet.setText(time.plusDays(4).format(format).toString());
		lblDateSub.setText(time.plusDays(5).format(format).toString());
		
		// remember which gridpane is colored so we can reset it later
		previousPane = parent;
	}
	
}
