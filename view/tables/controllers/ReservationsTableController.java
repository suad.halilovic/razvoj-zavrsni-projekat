package view.tables.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import api.Api;
import entities.Building;
import entities.Reservation;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import view.dialogs.controllers.EditReservationDeanController;



public class ReservationsTableController implements Initializable {
	@FXML
	private TableView<Reservation> tableView;
    @FXML
	private TableColumn<Reservation, String> descriptionColumn;
	@FXML
	private TableColumn<Reservation, String> teacherColumn;
	@FXML
	private TableColumn<Reservation, String> classroomColumn;
	@FXML
	private TableColumn<Reservation, String> dateColumn;
	@FXML
	private TableColumn<Reservation, String> startHourColumn;
	@FXML
	private TableColumn<Reservation, String> durationColumn;
	@FXML
	private Button btnAddItem;
	@FXML
	private Button btnDeleteItem;

	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
            descriptionColumn.setCellValueFactory(new PropertyValueFactory<Reservation, String>("description"));
	        teacherColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getTeacher().toString());});
	        classroomColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getClassroom().getName());});
	        dateColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getDate().toString());});
	        startHourColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(new Integer(cellData.getValue().getStartHour()).toString());});
	        durationColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(new Integer(cellData.getValue().getDuration()).toString());});

	        tableView.setItems(FXCollections.observableArrayList(Api.getReservations()));
	        tableView.setEditable(true);
	        
            tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            
        }    
	 
		@FXML
	    public void handleEdit(CellEditEvent edittedCell){
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)tableView.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editReservationDean.fxml"));
		        Parent root = loader.load();
		        EditReservationDeanController editReservationController = loader.getController();
		        editReservationController.setReservation(tableView.getSelectionModel().getSelectedItem());
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Uredi rezervaciju");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
	    }
		
		@FXML 
		public void handleBtnAddItem() {
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)btnAddItem.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editReservationDean.fxml"));
		        Parent root = loader.load();
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Dodaj rezervaciju");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
		}
		
		private void rerender() {
	        tableView.setItems(FXCollections.observableArrayList(Api.getReservations()));
		}
	
	@FXML
	public void handleBtnDeleteItem() {
	
	    Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati rezervaciju?",
				bool -> {
					if(bool) {
						 ObservableList<Reservation> selectedRows = tableView.getSelectionModel().getSelectedItems();

						    for (Reservation reservation : selectedRows) {
					            Api.deleteReservation(reservation);
					            rerender();
						    }
					}});
		
}

}
